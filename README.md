# vala-compile-resources
gresources compiler

```
Help Options:
  -h, --help                   Show help options

Application Options:
  -p, --prefix=PREFIX          resource prefix
  -r, --resource-file=FILE     resource file
  -t, --target=FILENAME        target
```

prerequisites :
- [meson](https://github.com/mesonbuild/meson)
- [ninja](https://github.com/ninja-build/ninja)
- [valac](https://git.gnome.org/browse/vala/)

building :
```
cd vala-compile-resources
mkdir build && cd build
meson ..
ninja
sudo ninja install
```
