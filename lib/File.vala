namespace Vcr {
	public class File : Object, Gee.Hashable<File> {
		public bool equal_to (File f) {
			return file.equal (f.file);
		}
		
		public uint hash() {
			return file.hash();
		}
		
		public GLib.File file { get; construct; }
		
		public static File open (GLib.File f) throws Error {
			if (!f.query_exists())
				throw new IOError.NOT_FOUND ("'%s' file not found.".printf (f.get_path()));
			return (File)Object.new (typeof (File), "file", f);
		}
		
		public static File open_path (string path) throws Error {
			return open (GLib.File.new_for_path (path));
		}
	}
}
