namespace Vcr {
	public class CompileResult : Object {
		internal CompileResult (string xml, string data) {
			Object (xml : xml, data : data);
		}
		
		public string xml { get; construct; }
		
		public string data { get; construct; }
	}
}
