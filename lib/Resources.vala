namespace Vcr {
	public class Resources : Object {
		Gee.ArrayList<Resource> rlist;
		
		construct {
			rlist = new Gee.ArrayList<Resource>();
		}
		
		internal string to_string() {
			var res = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<gresources>\n";
			foreach (var rsc in rlist) {
				res += "\t<gresource prefix=\"%s\">\n".printf (rsc.prefix);
				foreach (var file in rsc.files)
					res += "\t\t<file>%s</file>\n".printf (file.file.get_basename());
				res += "\t</gresource>\n";
			}
			res += "</gresources>\n";
			return res;
		}
		
		public CompileResult compile() throws Error {
			int idx = 0;
			var content = "";
			FileIOStream st1 = null, st2 = null;
			var f1 = GLib.File.new_tmp (null, out st1);
			var f2 = GLib.File.new_tmp (null, out st2);
			st1.output_stream.write (to_string().data);
			var plist = new Gee.ArrayList<string>();
			plist.add_all_array ({ "glib-compile-resources", "--target", f2.get_path() });
			foreach (var rsc in rlist)
				plist.add_all_array ({ "--sourcedir", rsc.sourcedir });
			plist.add_all_array ({ "--generate-source", f1.get_path() });
			plist.add (null);
			var proc = new Subprocess.newv (plist.to_array(), 
				SubprocessFlags.STDERR_PIPE | SubprocessFlags.STDIN_PIPE | SubprocessFlags.STDOUT_PIPE);
			proc.wait();
			FileUtils.get_contents (f2.get_path(), out content);
			f1.delete();
			f2.delete();
			return new CompileResult (to_string(), content);
		}
		
		public Gee.List<Resource> resources {
			get {
				return rlist;
			}
		}
	}
}
