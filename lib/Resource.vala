namespace Vcr {
	public class Resource : Object {
		Gee.ArrayList<File> list;
		
		construct {
			list = new Gee.ArrayList<File>();
		}
		
		public Resource (string prefix) {
			Object (prefix : prefix);
		}
		
		public string prefix { get; construct; }
		
		public string sourcedir { get; set; }
		
		public Gee.List<File> files {
			get {
				return list;
			}
		}
		
		public static Resource open (string path, string prefix) throws Error {
			var f = GLib.File.new_for_path (path);
			if (!f.query_exists())
				throw new IOError.NOT_FOUND ("resource filelist not found");
			if (prefix.strip().length == 0)
				throw new IOError.INVALID_DATA ("resource prefix cannot be null or empty");
			var rsc = new Resource (prefix.strip());
			rsc.sourcedir = f.get_parent().get_path();
			var dis = new DataInputStream (f.read());
			var line = "";
			while ((line = dis.read_line()) != null) {
				line = line.strip();
				if (line[0] != '/')
					line = rsc.sourcedir + "/" + line;
				rsc.files.add (File.open_path (line));
			}
			return rsc;
		}
		
		static Gee.List<File> get_files_ (GLib.File dir, bool recursive = true) throws Error {
			var list = new Gee.ArrayList<File>();
			var e = dir.enumerate_children ("standard::*", FileQueryInfoFlags.NOFOLLOW_SYMLINKS);
			FileInfo info = null;
			while ((info = e.next_file()) != null) {
				var child = e.get_child (info);
				if ((info.get_file_type() == FileType.DIRECTORY) && recursive)
					list.add_all (get_files_ (child, true));
				else if (info.get_file_type() == FileType.REGULAR)
					list.add (File.open (child));
			}
			return list;
		}
		
		public static Resource open_directory (string path, string prefix, bool recursive = true) throws Error {
			var dir = GLib.File.new_for_path (path);
			var rsc = new Resource (prefix);
			rsc.sourcedir = dir.get_path();
			rsc.files.add_all (get_files_ (dir, recursive));
			return rsc;
		}
	}
}
