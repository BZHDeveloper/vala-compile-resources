from . import ExtensionModule, ModuleReturnValue
from .. import build, mlog, mesonlib
from ..interpreterbase import permittedKwargs

class ValaModule(ExtensionModule):
    @permittedKwargs({ 'resource_file', 'prefix' })
    def compile_resources(self, state, args, kwargs):
        resource_file = kwargs.pop('resource_file', None)
        prefix = kwargs.pop('prefix', None)
        if not isinstance(resource_file, str):
            raise MesonException('resource_file must be a string')
        if not isinstance(prefix, str):
            raise MesonException('prefix must be a string')
        command = [ 'vala-compile-resources', 
            '--resource-file', resource_file,
            '--prefix', prefix,
            '--target', '@OUTPUT@' ]
        custom_kwargs = {
            'command' : command,
            'output' : args[0] + "-resources.c"
        }
        ct = build.CustomTarget(args[0] + '_resources', state.subdir, state.subproject, custom_kwargs)
        command = [ 'vala-compile-resources', 
            '--resource-file', resource_file,
            '--prefix', prefix,
            '--xml', '@OUTPUT@',
            '--target', 'osef.c' ]
        custom_kwargs = {
            'command' : command,
            'output' : args[0] + "-resources.xml"
        }
        ctx = build.CustomTarget(args[0] + '_resources_xml', state.subdir, state.subproject, custom_kwargs)
        rv = [ ct, ctx ]
        return ModuleReturnValue(rv,rv)

def initialize(*args, **kwargs):
    return ValaModule(*args, **kwargs)
