Gee.List<string> pointer_to_list (char** ptr) {
	var list = new Gee.ArrayList<string>();
	if (ptr == null)
		return list;
	var i = 0;
	while (ptr[i] != null) {
		list.add ((string)ptr[i]);
		i++;
	}
	return list;
}

string generate_filename() {
	var list = new Gee.ArrayList<uint8>();
	list.add ((uint8)(65 + Random.int_range (0,26)));
	while (FileUtils.test ("%s.xml".printf ((string)list.to_array()), FileTest.EXISTS))
		list.add ((uint8)(65 + Random.int_range (0,26)));
	return "%s.xml".printf ((string)list.to_array());
}

int main (string[] args) {
	string target = "", xml = null;
	char** pptr = null, fptr = null;
	OptionEntry[] options = {
		{ "target", 't', 0, OptionArg.FILENAME, ref target, "target", "FILENAME" },
		{ "xml", 'x', 0, OptionArg.FILENAME, ref xml, "name of resource xml", "FILENAME" },
		{ "prefix", 'p', 0, OptionArg.STRING_ARRAY, ref pptr, "resource prefix", "PREFIX" },
		{ "resource-file", 'r', 0, OptionArg.FILENAME_ARRAY, ref fptr, "resource file", "FILE" }
	};
	
	var opt_context = new OptionContext (" - vala-compile-resources");
	opt_context.set_help_enabled (true);
	opt_context.add_main_entries (options, null);
	try {
		opt_context.parse (ref args);
	}
	catch {
		return -1;
	}
	var prefixes = pointer_to_list (pptr);
	var files = pointer_to_list (fptr);
	if (xml == null)
		xml = generate_filename();
	if (target == null || target.strip().length == 0) {
		stderr.printf ("'target' argument cannot be null or empty\n");
		return -2;
	}
	if (files.size == 0) {
		stderr.printf ("'resource-file' argument must be provided\n");
		return -2;
	}
	if (prefixes.size == 0) {
		stderr.printf ("'prefix' argument must be provided\n");
		return -2;
	}
	if (prefixes.size != files.size) {
		stderr.printf ("'prefix' and 'resource-file' arguments must be of the same length\n");
		return -2;
	}
	var rscs = new Vcr.Resources();
	for (var i = 0; i < files.size; i++)
		try {
			var rsc = Vcr.Resource.open (files[i], prefixes[i]);
			rscs.resources.add (rsc);
		}
		catch (Error e) {
			stderr.printf (e.message + "\n");
			return -3;
		}
	try {
		var result = rscs.compile();
		FileUtils.set_contents (xml, result.xml);
		if (target == "-")
			print (result.data);
		else
			FileUtils.set_contents (target, result.data);
	}
	catch (Error e) {
		stderr.printf (e.message + "\n");
		return -4;
	}
	return 0;
}
